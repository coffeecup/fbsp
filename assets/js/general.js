var general = (function() {
  'use strict';

  var config = {
    selectors: {
      share: '#share'
    }
  }

  function init() {
  }

  // function validate_forms() {
  //   $(config.selectors.form).each(function() {
  //     $(this).validate(config.validate_options);
  //   });
  // }

  // function maskInputs(){
  //   $(config.selectors.cpf_field).mask(config.mask_options.cpf);
  //   $(config.selectors.data_field).mask(config.mask_options.data);
  //   $(config.selectors.tel_field).mask(config.mask_options.tel);
  // }

  // function updateSocialField()
  // {
  //   var campanhaValue = $(config.selectors.campanha_field).val();
  //   var escolaValue = $(config.selectors.escola_field).val();
  //   var jsonValue = {
  //     escola: escolaValue,
  //     campanha: campanhaValue
  //   };

  //   var jsonToString = JSON.stringify(jsonValue);
  //   $(config.selectors.social_field).val(jsonToString);
  // }

  // function social_share() {
  //   $(config.selectors.share).jsSocials({
  //     showCount: false,
  //     showLabel: false,
  //     shares: ['facebook', 'twitter', 'googleplus', 'linkedin', 'pinterest','email']
  //   });
  // }

  return {
    init: init
  };
}());

$(document).ready(function() {
  general.init();
});
