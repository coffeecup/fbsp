(function ($) {
    //Valida CPF
    $.validator.addMethod('cpf',function(value,element,param) {
       $return = true;

       // this is mostly not needed
       var invalidos = [
            '111.111.111-11',
            '222.222.222-22',
            '333.333.333-33',
            '444.444.444-44',
            '555.555.555-55',
            '666.666.666-66',
            '777.777.777-77',
            '888.888.888-88',
            '999.999.999-99',
            '000.000.000-00'
        ];
        for(i=0;i<invalidos.length;i++) {
            if( invalidos[i] == value) {
                $return = false;
            }
        }

        value = value.replace("-","");
        value = value.replace(/\./g,"");

        //validando primeiro digito
        add = 0;
        for ( i=0; i < 9; i++ ) {
            add += parseInt(value.charAt(i), 10) * (10-i);
        }
        rev = 11 - ( add % 11 );
        if( rev == 10 || rev == 11) {
            rev = 0;
        }
        if( rev != parseInt(value.charAt(9), 10) ) {
            $return = false;
        }

        //validando segundo digito
        add = 0;
        for ( i=0; i < 10; i++ ) {
            add += parseInt(value.charAt(i), 10) * (11-i);
        }
        rev = 11 - ( add % 11 );
        if( rev == 10 || rev == 11) {
            rev = 0;
        }
        if( rev != parseInt(value.charAt(10), 10) ) {
            $return = false;
        }

        return $return;
    });

    //Valida Email completo - necessário @ e .
    $.validator.addMethod("emailfull", function(value, element) {
        return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
    }, "Please enter valid email address!");

    //Permite apenas letras e espaços
    // $.validator.addMethod("lettersonly", function(value, element) {
    //     return this.optional(element) || /^[a-z\s]+$/i.test(value);
    // }, "Only alphabetical characters");

})(jQuery);