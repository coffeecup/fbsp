set :application, 'fbsp'
set :repo_url, 'git@bitbucket.org:coffeecup/fbsp.git'


# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# set :linked_files, fetch(:linked_files, []).push('wp-config.php')

# set :linked_dirs, fetch(:linked_dirs, [])
#     .push("wp-content/uploads")

set :keep_releases, 2

namespace :deploy do

  desc 'move environment file'
  task :vai_fbsp do
    on roles(:staging), in: :sequence, wait: 5 do
      #execute "cd #{release_path}; composer install"
      #execute "cd #{release_path}; cp wp-config-sample.php wp-config.php;"
    end
    on roles(:production), in: :sequence, wait: 5 do
      # execute "cd #{release_path}; mv .env.production .env;"

      # Commands to help W3 Total Cache
      # execute "mkdir #{release_path}/wp-content/w3tc-config"
      # execute "chmod 777 #{release_path}/wp-content/w3tc-config"
      # execute "chmod 777 #{release_path}/nginx.conf"
      # execute "cp #{release_path}/wp-content/plugins/w3-total-cache/wp-content/advanced-cache.php #{release_path}/wp-content/advanced-cache.php"
      # execute "mkdir #{release_path}/wp-content/cache"
      # execute "chmod 777 #{release_path}/wp-content/cache"
      # execute "cp #{release_path}/wp-content/plugins/w3-total-cache/wp-content/db.php #{release_path}/wp-content/db.php"
    end
  end

  before :publishing, :vai_fbsp
  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end

    on roles(:staging), in: :sequence, wait: 5 do
      execute "cd #{release_path} && npm install"
    end

    on roles(:production), in: :sequence, wait: 5 do
    end
  end

end
