var gulp = require('gulp'),
    compass = require('gulp-compass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    rename = require('gulp-rename'),
    include = require('gulp-include'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    theme = "assets",
    path = {
      css: theme + "/css",
      js: theme + "/js",
      scss: theme + "/sass",
      images: theme + "/images",
      fonts: theme + "/fonts"
    };

// Error
function onError(err) {
  console.log(err);
  this.emit('end');
}

gulp.task('build', ['styles', 'scripts']);
gulp.task('default', ['build', 'watch']);

gulp.task('styles', function() {
  gulp.src(path.scss + '/**/*.scss')
    .pipe(compass({
      require: ['sass-globbing', 'compass/import-once/activate'],
      sourcemap: true,
      css: path.css,
      font: path.fonts,
      sass: path.scss,
      image: path.images,
      style: "nested",
      comments: false,
      relative: true
    }))
    .on('error', onError)
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .on('error', onError)
    .pipe(autoprefixer({browsers: ['last 10 versions'], cascade: false }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(path.css));
})

gulp.task('scripts', function() {
  return gulp.src([path.js + '/**/*.js', '!' + path.js + '/**/*.min.js'])
    .pipe(include())
    .pipe(uglify())
    .on('error', onError)
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest(path.js));
});

gulp.task('watch', function() {
  gulp.watch(path.scss + '/**/*.scss', ['styles'])
  gulp.watch([path.js + '/**/*.js', '!' + path.js + '/**/*.min.js'], ['scripts'])
});
